USE [master]
GO
/****** Object:  Database [SupervisorDb]    Script Date: 8/24/2020 10:01:09 PM ******/
CREATE DATABASE [SupervisorDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SupervisorDb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\SupervisorDb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SupervisorDb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\SupervisorDb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [SupervisorDb] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SupervisorDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SupervisorDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SupervisorDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SupervisorDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SupervisorDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SupervisorDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [SupervisorDb] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [SupervisorDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SupervisorDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SupervisorDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SupervisorDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SupervisorDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SupervisorDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SupervisorDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SupervisorDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SupervisorDb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SupervisorDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SupervisorDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SupervisorDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SupervisorDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SupervisorDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SupervisorDb] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [SupervisorDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SupervisorDb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SupervisorDb] SET  MULTI_USER 
GO
ALTER DATABASE [SupervisorDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SupervisorDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SupervisorDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SupervisorDb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SupervisorDb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SupervisorDb] SET QUERY_STORE = OFF
GO
USE [SupervisorDb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 8/24/2020 10:01:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Classes]    Script Date: 8/24/2020 10:01:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Classes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Classes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClassStudents]    Script Date: 8/24/2020 10:01:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassStudents](
	[StudentID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
 CONSTRAINT [PK_ClassStudents] PRIMARY KEY CLUSTERED 
(
	[ClassID] ASC,
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ResearchProjects]    Script Date: 8/24/2020 10:01:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResearchProjects](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[StudentID] [int] NOT NULL,
 CONSTRAINT [PK_ResearchProjects] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Students]    Script Date: 8/24/2020 10:01:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Students](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[DOB] [datetime2](7) NOT NULL,
	[Subject] [nvarchar](max) NULL,
	[SupervisorID] [int] NOT NULL,
 CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Supervisors]    Script Date: 8/24/2020 10:01:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supervisors](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[DOB] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Supervisors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_ClassStudents_StudentID]    Script Date: 8/24/2020 10:01:09 PM ******/
CREATE NONCLUSTERED INDEX [IX_ClassStudents_StudentID] ON [dbo].[ClassStudents]
(
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_ResearchProjects_StudentID]    Script Date: 8/24/2020 10:01:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_ResearchProjects_StudentID] ON [dbo].[ResearchProjects]
(
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Students_SupervisorID]    Script Date: 8/24/2020 10:01:09 PM ******/
CREATE NONCLUSTERED INDEX [IX_Students_SupervisorID] ON [dbo].[Students]
(
	[SupervisorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClassStudents]  WITH CHECK ADD  CONSTRAINT [FK_ClassStudents_Classes_ClassID] FOREIGN KEY([ClassID])
REFERENCES [dbo].[Classes] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ClassStudents] CHECK CONSTRAINT [FK_ClassStudents_Classes_ClassID]
GO
ALTER TABLE [dbo].[ClassStudents]  WITH CHECK ADD  CONSTRAINT [FK_ClassStudents_Students_StudentID] FOREIGN KEY([StudentID])
REFERENCES [dbo].[Students] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ClassStudents] CHECK CONSTRAINT [FK_ClassStudents_Students_StudentID]
GO
ALTER TABLE [dbo].[ResearchProjects]  WITH CHECK ADD  CONSTRAINT [FK_ResearchProjects_Students_StudentID] FOREIGN KEY([StudentID])
REFERENCES [dbo].[Students] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ResearchProjects] CHECK CONSTRAINT [FK_ResearchProjects_Students_StudentID]
GO
ALTER TABLE [dbo].[Students]  WITH CHECK ADD  CONSTRAINT [FK_Students_Supervisors_SupervisorID] FOREIGN KEY([SupervisorID])
REFERENCES [dbo].[Supervisors] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Students] CHECK CONSTRAINT [FK_Students_Supervisors_SupervisorID]
GO
USE [master]
GO
ALTER DATABASE [SupervisorDb] SET  READ_WRITE 
GO
