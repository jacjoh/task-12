USE [SupervisorDb]
GO

INSERT INTO [dbo].[Classes]
           ([Name])
     VALUES
           ('Dotnet'),
		   ('Java'),
		   ('JavaScript')
GO


INSERT INTO [dbo].[Supervisors]
           ([FirstName], [LastName], [DOB])
     VALUES
           ('Klaudia', 'Cleveland', DATETIME2FROMPARTS(2000, 12, 31, 23, 59, 59, 0, 0 )),
           ('Mollie', 'Britton', DATETIME2FROMPARTS(1994, 10, 10, 23, 59, 59, 0, 0 )),
           ('Gurleen', 'Mclellan', DATETIME2FROMPARTS(1990, 01, 01, 23, 59, 59, 0, 0 ))
GO


