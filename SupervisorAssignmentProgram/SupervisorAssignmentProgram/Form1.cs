﻿using Newtonsoft.Json;
using SupervisorAssignmentProgram.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SupervisorAssignmentProgram
{
    public partial class Form1 : Form
    {
        private bool NewStudent = true;
        
        //Used to temporarely store student data
        private Student selectedStudent = new Student();
        public Form1()
        {
            InitializeComponent();

            //Fills supervisor components with data
            List<Supervisor> supervisors =  DbHandler.GetSupervisors();
            foreach(Supervisor supervisor in supervisors)
            {
                cbSupervisors.Items.Add(supervisor.ID + " " + supervisor.FirstName + " " + supervisor.LastName);
                cbStudentSupervisor.Items.Add(supervisor.ID + " " + supervisor.FirstName + " " + supervisor.LastName);
            }

            //Fills class components with data
            List<Class> classes = DbHandler.GetClasses();
            foreach (Class @class in classes)
            {
                lbClasses.Items.Add(@class.ID + " " + @class.Name);
                cbNewClasses.Items.Add(@class.ID + " " + @class.Name);
            }
        }

        //Get students for given supervisor
        private void cbSupervisors_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Student> students = DbHandler.GetStudentsForSupervisor(Int32.Parse(cbSupervisors.SelectedItem.ToString().Split()[0]));
            lbStudents.Items.Clear();
            foreach(Student student in students)
            {
                lbStudents.Items.Add(student.ID + " " + student.FirstName + " " + student.LastName);
            }
        }

        //When selecting a student fill the components with relevant data
        private void lbStudents_SelectedIndexChanged(object sender, EventArgs e)
        {
            Student student = DbHandler.GetStudent(Int32.Parse(lbStudents.SelectedItem.ToString().Split(" ")[0]));

            //Student data
            tbFirstname.Text = student.FirstName;
            tbLastname.Text = student.LastName;
            tbSubject.Text = student.Subject;
            dtpDOB.Value = student.DOB;
            cbStudentSupervisor.SelectedItem = student.Supervisor.ID + " " + student.Supervisor.FirstName + " " + student.Supervisor.LastName;

            //A student does not need to have a research project, and can not fill a textbox with null
            if(student.ResearchProject != null)
            {
                tbResearchProject.Text = student.ResearchProject.Title;
            }
            else
            {
                tbResearchProject.Text = "";
            }

            //Fill the listbox for the student with classes that they attend to
            lbStudentClasses.Items.Clear();
            foreach(ClassStudent classStudent in student.ClassStudents)
            {
                lbStudentClasses.Items.Add(classStudent.Class.ID + " " + classStudent.Class.Name);
            }

            //Set the selectedstudent and now allows updates
            selectedStudent = student;
            NewStudent = false;
        }

        private void lbClasses_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Get students for the given class
            List<Student> students = DbHandler.GetStudentsForClass(Int32.Parse(lbClasses.SelectedItem.ToString().Split()[0]));
            lbStudents.Items.Clear();
            foreach (Student student in students)
            {
                lbStudents.Items.Add(student.ID + " " + student.FirstName + " " + student.LastName);
            }
        }

        private void btnUpdateStudent_Click(object sender, EventArgs e)
        {
            // Cannot update a new character
            if (NewStudent)
            {
                MessageBox.Show("Cannot update. Either create a student or select a student");
                return;
            }
            DbHandler.UpdateStudent(selectedStudent);
        }

        //Sets the students first name
        private void tbFirstname_TextChanged(object sender, EventArgs e)
        {
            selectedStudent.FirstName = tbFirstname.Text;
        }

        //Sets the students last name
        private void tbLastname_TextChanged(object sender, EventArgs e)
        {
            selectedStudent.LastName = tbLastname.Text;
        }

        //Sets the students subject
        private void tbSubject_TextChanged(object sender, EventArgs e)
        {
            selectedStudent.Subject = tbSubject.Text;
        }

        //Sets the students date of birth
        private void dtpDOB_ValueChanged(object sender, EventArgs e)
        {
            selectedStudent.DOB = dtpDOB.Value;
        }

        //Sets the students supervisor
        private void cbStudentSupervisor_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedStudent.SupervisorID = Int32.Parse(cbStudentSupervisor.SelectedItem.ToString().Split(" ")[0]);
        }

        //Deletes a student and clears all the components
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //Checks if there is a student selected
            if(lbStudents.SelectedItem == null)
            {
                MessageBox.Show("Must select a student to delete");
                return;
            }
            DbHandler.DeleteStudent(selectedStudent);
            ClearComponents();
        }

        private void ClearSelectedStudent()
        {
            selectedStudent = new Student();
        }

        //Clears all components of data
        private void ClearComponents()
        {
            ClearSelectedStudent();

            tbFirstname.Text = "";
            tbLastname.Text = "";
            tbSubject.Text = "";
            dtpDOB.Value = DateTime.Now;

            cbStudentSupervisor.SelectedItem = "";
            tbResearchProject.Text = "";

            lbStudentClasses.Items.Clear();

            NewStudent = true;
        }

        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            //Checks if all the parameters are present
            if(!CheckParameter())
            {
                return;
            }
            
            //Check if it is supposed to be a new student or if there are selected a student
            if (!NewStudent)
            {
                MessageBox.Show("Cannot add a new student when there are a selected character" +
                    "Either update or press new student");
                return;
            }

            //Create an object with data from components
            Student student = new Student()
            {
                FirstName = tbFirstname.Text,
                LastName = tbLastname.Text,
                Subject = tbSubject.Text,
                DOB = dtpDOB.Value,
                SupervisorID = Int32.Parse(cbStudentSupervisor.SelectedItem.ToString().Split(" ")[0])
            };

            //Add the student to the db
            selectedStudent = DbHandler.AddStudent(student);
            MessageBox.Show($"Successfully added {student.FirstName} {student.LastName} To {cbStudentSupervisor.SelectedItem.ToString().Split(" ")[1]}\n" +
                $"You can assign a research project to the student and assign classes to the student");
        }

        //Checks if all the parameters are present
        private bool CheckParameter()
        {
            if (tbFirstname.Text == "")
            {
                MessageBox.Show("You Must specify the first name");
                return false;
            }
            else if (tbLastname.Text == "")
            {
                MessageBox.Show("You must specify the last name");
                return false;
            }
            else if (tbSubject.Text == "")
            {
                MessageBox.Show("You must specify the subject");
                return false;
            }
            else if (0 < DateTime.Compare(dtpDOB.Value, DateTime.Now))
            {
                MessageBox.Show("You must specify the date of birth");
                return false;
            }
            else if (cbStudentSupervisor.SelectedItem.ToString() == "")
            {
                MessageBox.Show("You must specify the supervisor for the student");
                return false;
            }
            else
            {
                return true;
            }
        }

        private void tbResearchProject_TextChanged(object sender, EventArgs e)
        {
            //Update the research project in the student class if there is a project available
            if(selectedStudent.ResearchProject != null)
            {
                selectedStudent.ResearchProject.Title = tbResearchProject.Text;
            }
        }

        //Creates a new research
        private void btnResearchNew_Click(object sender, EventArgs e)
        {
            //Creates a new research project if there are none available, and there are added a project
            if(selectedStudent.ResearchProject == null && tbResearchProject.Text != "")
            {
                ResearchProject research = new ResearchProject() { Title = tbResearchProject.Text, StudentID = selectedStudent.ID };
                selectedStudent.ResearchProject = research;
            }
            DbHandler.AddResearchProject(selectedStudent);
        }

        //Updates the research project
        private void btnUpdateProject_Click(object sender, EventArgs e)
        {
            //Cannot update a non-existing project
            if (selectedStudent.ResearchProject != null)
            {
                DbHandler.UpdateResearchProject(selectedStudent);
            }
            else
            {
                MessageBox.Show("Must add a research project first");
            }
        }

        //Deletes a research project
        private void btnDeleteProject_Click(object sender, EventArgs e)
        {
            //Cannot delete a non-existing project
            if(selectedStudent.ResearchProject != null)
            {
                DbHandler.DeleteResearchProject(selectedStudent);
                tbResearchProject.Text = "";
            }
            else
            {
                MessageBox.Show("There is no research project to delete");
            }
        }

        //Set the selected item
        private void cbNewClasses_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbNewClasses.SelectedItem = cbNewClasses.SelectedItem.ToString();
        }

        //Adds a class to the student 
        private void btnAddClass_Click(object sender, EventArgs e)
        {
            //Parses the id of the class
            bool parsable = Int32.TryParse(cbNewClasses.SelectedItem.ToString().Split(" ")[0], out int classID);
            if(!parsable)
            {
                MessageBox.Show("Could not parse the id");
                return;
            }

            //Adds the class to the student and updates the db
            ClassStudent classStudent = new ClassStudent() { ClassID = classID, StudentID = selectedStudent.ID };
            selectedStudent.ClassStudents.Add(classStudent);
            selectedStudent = DbHandler.AddClassToStudent(selectedStudent, classStudent);
        }

        //Removes a class from the student
        private void btnRemoveClass_Click(object sender, EventArgs e)
        {
            //Cannot remove nothing from the db
            if(lbStudentClasses.SelectedItem == null)
            {
                MessageBox.Show("Must choose a class to remove");
                return;
            }

            selectedStudent = DbHandler.RemoveClassFromStudent(selectedStudent,
                Int32.Parse(lbStudentClasses.SelectedItem.ToString().Split(" ")[0]));
        }

        //Writes the supervisor class with all the data to a json file
        private void btnJSON_Click(object sender, EventArgs e)
        {
            //gets supervisors with all the data from the other classes
            List<Supervisor> supervisors = DbHandler.GetSupervisorWithAllData();

            //Convert the objects to json
            string JSONsupervisors = JsonConvert.SerializeObject(supervisors, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            //Write the json object to a file
            using (StreamWriter file = File.CreateText("JSONFile.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                //serialize object directly into file stream
                serializer.Serialize(file, JSONsupervisors);
            }
        }

        //Clears all components so that one can create a new student
        private void btnNewStudent_Click(object sender, EventArgs e)
        {
            ClearComponents();
        }
    }
}
