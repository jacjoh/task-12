using SupervisorAssignmentProgram.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SupervisorAssignmentProgram
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //AddData();
            Application.Run(new Form1());
        }
        
        /*
        static void AddData()
        {
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                supervisorsDbContext.Add(new Supervisor() { FirstName = "John", LastName = "Johnsen", DOB = new DateTime(1990, 10, 10) });
                supervisorsDbContext.Add(new Supervisor() { FirstName = "Arne", LastName = "Arntsen", DOB = new DateTime(1980, 1, 1) });
                supervisorsDbContext.Add(new Class() { Name = "DotNet" });
                supervisorsDbContext.Add(new Student() { FirstName = "Julie", LastName = "Jansen", DOB = new DateTime(1998, 2, 3), SupervisorID = 3 });
                supervisorsDbContext.Add(new Student() { FirstName = "Sondre", LastName = "Jansen", DOB = new DateTime(1998, 2, 3), SupervisorID = 3 });
                supervisorsDbContext.Add(new ResearchProject() { Title = "RPG", StudentID = 2 });
                supervisorsDbContext.Add(new ClassStudent() { StudentID = 2, ClassID = 2 });
                supervisorsDbContext.Add(new ClassStudent() { StudentID = 3, ClassID = 2 });

                supervisorsDbContext.SaveChanges();
            }
        }
        */
    }
}
