﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SupervisorAssignmentProgram.Migrations
{
    public partial class AddedKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassStudent_Classes_ClassID",
                table: "ClassStudent");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassStudent_Students_StudentID",
                table: "ClassStudent");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassStudent",
                table: "ClassStudent");

            migrationBuilder.RenameTable(
                name: "ClassStudent",
                newName: "ClassStudents");

            migrationBuilder.RenameIndex(
                name: "IX_ClassStudent_StudentID",
                table: "ClassStudents",
                newName: "IX_ClassStudents_StudentID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassStudents",
                table: "ClassStudents",
                columns: new[] { "ClassID", "StudentID" });

            migrationBuilder.AddForeignKey(
                name: "FK_ClassStudents_Classes_ClassID",
                table: "ClassStudents",
                column: "ClassID",
                principalTable: "Classes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassStudents_Students_StudentID",
                table: "ClassStudents",
                column: "StudentID",
                principalTable: "Students",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassStudents_Classes_ClassID",
                table: "ClassStudents");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassStudents_Students_StudentID",
                table: "ClassStudents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassStudents",
                table: "ClassStudents");

            migrationBuilder.RenameTable(
                name: "ClassStudents",
                newName: "ClassStudent");

            migrationBuilder.RenameIndex(
                name: "IX_ClassStudents_StudentID",
                table: "ClassStudent",
                newName: "IX_ClassStudent_StudentID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassStudent",
                table: "ClassStudent",
                columns: new[] { "ClassID", "StudentID" });

            migrationBuilder.AddForeignKey(
                name: "FK_ClassStudent_Classes_ClassID",
                table: "ClassStudent",
                column: "ClassID",
                principalTable: "Classes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassStudent_Students_StudentID",
                table: "ClassStudent",
                column: "StudentID",
                principalTable: "Students",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
