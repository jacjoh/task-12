﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using SupervisorAssignmentProgram.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SupervisorAssignmentProgram
{
    public class DbHandler
    {
        //Gets all supervisors and returns them in a list
        public static List<Supervisor> GetSupervisors()
        {
            List<Supervisor> supervisors = new List<Supervisor>();
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                supervisors = supervisorsDbContext.Supervisors.ToList();
            }
            return supervisors;
        }

        //Gets all the relevant data for a student selected
        public static Student GetStudent(int studentId)
        {
            Student student = new Student();

            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                student = supervisorsDbContext.Students.Include(s => s.ResearchProject).Include(s => s.Supervisor)
                    .Include(s => s.ClassStudents).ThenInclude(cs => cs.Class).Where(s => s.ID == studentId).FirstOrDefault();
            }
            return student;
        }

        //Gets all the students for a supervisor selected
        public static List<Student> GetStudentsForSupervisor(int supervisorId)
        {
            List<Student> students = new List<Student>();
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                students = supervisorsDbContext.Students.Where(s => s.SupervisorID == supervisorId).ToList();
            }
            return students;
        }

        //Gets all the students for a class selected
        public static List<Student> GetStudentsForClass(int classId)
        {
            List<Student> students = new List<Student>();
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                students = supervisorsDbContext.Students.Where(s => s.ClassStudents.Any(cs => cs.ClassID == classId)).ToList();
            }
            return students;
        }

        //Gets all the classes
        public static List<Class> GetClasses()
        {
            List<Class> classes = new List<Class>();
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                classes = supervisorsDbContext.Classes.ToList();
            }
            return classes;
        }

        //Adds a research project to the db, and adds a pointer to the student
        public static void AddResearchProject(Student student)
        {
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                supervisorsDbContext.ResearchProjects.Add(new ResearchProject() { Title = student.ResearchProject.Title, StudentID = student.ID });
                supervisorsDbContext.SaveChanges();
            }
        }

        //Adds a class to a student by creating a middletable with pointers to both
        public static void SaveClassForStudent(Student student, Class @class)
        {
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                supervisorsDbContext.ClassStudents.Add(new ClassStudent() { ClassID = @class.ID, StudentID = student.ID });
                supervisorsDbContext.SaveChanges();
            }
        }

        //Updates the data for a student
        public static void UpdateStudent(Student student)
        {   
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                var updatedStudent = supervisorsDbContext.Students.Find(student.ID);
                updatedStudent.Subject = student.Subject;
                updatedStudent.FirstName = student.FirstName;
                updatedStudent.LastName = student.LastName;
                updatedStudent.DOB = student.DOB;
                updatedStudent.SupervisorID = student.SupervisorID;
                supervisorsDbContext.SaveChanges();
            }
        }

        //Deletes a student and their research project
        public static void DeleteStudent(Student student)
        {
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                supervisorsDbContext.Remove(student.ResearchProject);
                supervisorsDbContext.Remove(student);
                supervisorsDbContext.SaveChanges();
            }
        }

        //Adds a student to the db.
        public static Student AddStudent(Student student)
        {
            Student createdStudent = new Student();
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                supervisorsDbContext.Students.Add(student);
                supervisorsDbContext.SaveChanges();
            }
            return student;
        }

        //Updates the research project for a given student
        public static void UpdateResearchProject(Student student)
        {
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                var updatedResearch = supervisorsDbContext.ResearchProjects.Find(student.ResearchProject.ID);
                updatedResearch.Title = student.ResearchProject.Title;
                supervisorsDbContext.SaveChanges();
            }
        }

        //Removes a research project
        public static void DeleteResearchProject(Student student)
        {
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                supervisorsDbContext.Remove(student.ResearchProject);
                supervisorsDbContext.SaveChanges();
            }
        }

        //Adds a class to a student
        public static Student AddClassToStudent(Student student, ClassStudent classStudent)
        {
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                //Adds the class student and returns the updated student
                supervisorsDbContext.ClassStudents.Add(classStudent);
                student.ClassStudents = supervisorsDbContext.ClassStudents.Where(cs => cs.StudentID == student.ID).ToList();
                supervisorsDbContext.SaveChanges();
            }
            return student;
        }

        //Removes a class from the student object and returns the updated student
        public static Student RemoveClassFromStudent(Student student, int classId)
        {
            ClassStudent classStudent = student.ClassStudents.Where(cl => cl.ClassID == classId).FirstOrDefault();
            student.ClassStudents.Remove(classStudent);
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                supervisorsDbContext.Remove(classStudent);
                supervisorsDbContext.SaveChanges();
            }
            return student;
        }

        //Gets a supervisor with data about the other classes to write to json
        public static List<Supervisor> GetSupervisorWithAllData()
        {
            List<Supervisor> supervisors = new List<Supervisor>();
            using (SupervisorsDbContext supervisorsDbContext = new SupervisorsDbContext())
            {
                supervisors = supervisorsDbContext.Supervisors.Include(s => s.Students).ThenInclude(s => s.ResearchProject).
                    Include(s => s.Students).ThenInclude(s => s.ClassStudents).ThenInclude(cl => cl.Class).ToList();
            }
            return supervisors;
        }
    }
}
