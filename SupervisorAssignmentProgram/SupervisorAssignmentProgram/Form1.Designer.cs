﻿namespace SupervisorAssignmentProgram
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbSupervisors = new System.Windows.Forms.ComboBox();
            this.lSupervisor = new System.Windows.Forms.Label();
            this.tbFirstname = new System.Windows.Forms.TextBox();
            this.lbSubject = new System.Windows.Forms.Label();
            this.lbLastName = new System.Windows.Forms.Label();
            this.lbFirstName = new System.Windows.Forms.Label();
            this.tbLastname = new System.Windows.Forms.TextBox();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.lDOB = new System.Windows.Forms.Label();
            this.lbStudents = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSubject = new System.Windows.Forms.TextBox();
            this.btnAddStudent = new System.Windows.Forms.Button();
            this.btnUpdateStudent = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbStudentSupervisor = new System.Windows.Forms.ComboBox();
            this.tbResearchProject = new System.Windows.Forms.TextBox();
            this.lblResearchProject = new System.Windows.Forms.Label();
            this.lbStudentClasses = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnResearchNew = new System.Windows.Forms.Button();
            this.lbClasses = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnUpdateProject = new System.Windows.Forms.Button();
            this.btnDeleteProject = new System.Windows.Forms.Button();
            this.cbNewClasses = new System.Windows.Forms.ComboBox();
            this.btnRemoveClass = new System.Windows.Forms.Button();
            this.btnAddClass = new System.Windows.Forms.Button();
            this.btnJSON = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnNewStudent = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbSupervisors
            // 
            this.cbSupervisors.FormattingEnabled = true;
            this.cbSupervisors.Location = new System.Drawing.Point(12, 37);
            this.cbSupervisors.Name = "cbSupervisors";
            this.cbSupervisors.Size = new System.Drawing.Size(164, 28);
            this.cbSupervisors.TabIndex = 0;
            this.cbSupervisors.SelectedIndexChanged += new System.EventHandler(this.cbSupervisors_SelectedIndexChanged);
            // 
            // lSupervisor
            // 
            this.lSupervisor.AutoSize = true;
            this.lSupervisor.Location = new System.Drawing.Point(12, 14);
            this.lSupervisor.Name = "lSupervisor";
            this.lSupervisor.Size = new System.Drawing.Size(84, 20);
            this.lSupervisor.TabIndex = 1;
            this.lSupervisor.Text = "Supervisors";
            // 
            // tbFirstname
            // 
            this.tbFirstname.Location = new System.Drawing.Point(371, 64);
            this.tbFirstname.Name = "tbFirstname";
            this.tbFirstname.Size = new System.Drawing.Size(151, 27);
            this.tbFirstname.TabIndex = 2;
            this.tbFirstname.TextChanged += new System.EventHandler(this.tbFirstname_TextChanged);
            // 
            // lbSubject
            // 
            this.lbSubject.AutoSize = true;
            this.lbSubject.Location = new System.Drawing.Point(244, 175);
            this.lbSubject.Name = "lbSubject";
            this.lbSubject.Size = new System.Drawing.Size(58, 20);
            this.lbSubject.TabIndex = 1;
            this.lbSubject.Text = "Subject";
            // 
            // lbLastName
            // 
            this.lbLastName.AutoSize = true;
            this.lbLastName.Location = new System.Drawing.Point(244, 118);
            this.lbLastName.Name = "lbLastName";
            this.lbLastName.Size = new System.Drawing.Size(72, 20);
            this.lbLastName.TabIndex = 1;
            this.lbLastName.Text = "Lastname";
            // 
            // lbFirstName
            // 
            this.lbFirstName.AutoSize = true;
            this.lbFirstName.Location = new System.Drawing.Point(244, 67);
            this.lbFirstName.Name = "lbFirstName";
            this.lbFirstName.Size = new System.Drawing.Size(73, 20);
            this.lbFirstName.TabIndex = 1;
            this.lbFirstName.Text = "Firstname";
            // 
            // tbLastname
            // 
            this.tbLastname.Location = new System.Drawing.Point(371, 115);
            this.tbLastname.Name = "tbLastname";
            this.tbLastname.Size = new System.Drawing.Size(151, 27);
            this.tbLastname.TabIndex = 2;
            this.tbLastname.TextChanged += new System.EventHandler(this.tbLastname_TextChanged);
            // 
            // dtpDOB
            // 
            this.dtpDOB.Location = new System.Drawing.Point(371, 233);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(151, 27);
            this.dtpDOB.TabIndex = 3;
            this.dtpDOB.ValueChanged += new System.EventHandler(this.dtpDOB_ValueChanged);
            // 
            // lDOB
            // 
            this.lDOB.AutoSize = true;
            this.lDOB.Location = new System.Drawing.Point(244, 238);
            this.lDOB.Name = "lDOB";
            this.lDOB.Size = new System.Drawing.Size(94, 20);
            this.lDOB.TabIndex = 1;
            this.lDOB.Text = "Date of birth";
            // 
            // lbStudents
            // 
            this.lbStudents.FormattingEnabled = true;
            this.lbStudents.ItemHeight = 20;
            this.lbStudents.Location = new System.Drawing.Point(12, 263);
            this.lbStudents.Name = "lbStudents";
            this.lbStudents.Size = new System.Drawing.Size(164, 184);
            this.lbStudents.TabIndex = 4;
            this.lbStudents.SelectedIndexChanged += new System.EventHandler(this.lbStudents_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 240);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Existing students";
            // 
            // tbSubject
            // 
            this.tbSubject.Location = new System.Drawing.Point(371, 172);
            this.tbSubject.Name = "tbSubject";
            this.tbSubject.Size = new System.Drawing.Size(151, 27);
            this.tbSubject.TabIndex = 5;
            this.tbSubject.TextChanged += new System.EventHandler(this.tbSubject_TextChanged);
            // 
            // btnAddStudent
            // 
            this.btnAddStudent.Location = new System.Drawing.Point(244, 334);
            this.btnAddStudent.Name = "btnAddStudent";
            this.btnAddStudent.Size = new System.Drawing.Size(78, 29);
            this.btnAddStudent.TabIndex = 6;
            this.btnAddStudent.Text = "Add";
            this.btnAddStudent.UseVisualStyleBackColor = true;
            this.btnAddStudent.Click += new System.EventHandler(this.btnAddStudent_Click);
            // 
            // btnUpdateStudent
            // 
            this.btnUpdateStudent.Location = new System.Drawing.Point(339, 334);
            this.btnUpdateStudent.Name = "btnUpdateStudent";
            this.btnUpdateStudent.Size = new System.Drawing.Size(83, 29);
            this.btnUpdateStudent.TabIndex = 7;
            this.btnUpdateStudent.Text = "Update";
            this.btnUpdateStudent.UseVisualStyleBackColor = true;
            this.btnUpdateStudent.Click += new System.EventHandler(this.btnUpdateStudent_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(442, 334);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(80, 29);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(244, 291);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Supervisor";
            // 
            // cbStudentSupervisor
            // 
            this.cbStudentSupervisor.FormattingEnabled = true;
            this.cbStudentSupervisor.Location = new System.Drawing.Point(371, 288);
            this.cbStudentSupervisor.Name = "cbStudentSupervisor";
            this.cbStudentSupervisor.Size = new System.Drawing.Size(151, 28);
            this.cbStudentSupervisor.TabIndex = 10;
            this.cbStudentSupervisor.SelectedIndexChanged += new System.EventHandler(this.cbStudentSupervisor_SelectedIndexChanged);
            // 
            // tbResearchProject
            // 
            this.tbResearchProject.Location = new System.Drawing.Point(564, 300);
            this.tbResearchProject.Name = "tbResearchProject";
            this.tbResearchProject.Size = new System.Drawing.Size(218, 27);
            this.tbResearchProject.TabIndex = 11;
            this.tbResearchProject.TextChanged += new System.EventHandler(this.tbResearchProject_TextChanged);
            // 
            // lblResearchProject
            // 
            this.lblResearchProject.AutoSize = true;
            this.lblResearchProject.Location = new System.Drawing.Point(564, 277);
            this.lblResearchProject.Name = "lblResearchProject";
            this.lblResearchProject.Size = new System.Drawing.Size(118, 20);
            this.lblResearchProject.TabIndex = 12;
            this.lblResearchProject.Text = "Research Project";
            // 
            // lbStudentClasses
            // 
            this.lbStudentClasses.FormattingEnabled = true;
            this.lbStudentClasses.ItemHeight = 20;
            this.lbStudentClasses.Location = new System.Drawing.Point(565, 37);
            this.lbStudentClasses.Name = "lbStudentClasses";
            this.lbStudentClasses.Size = new System.Drawing.Size(150, 84);
            this.lbStudentClasses.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(564, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "Student Classes";
            // 
            // btnResearchNew
            // 
            this.btnResearchNew.Location = new System.Drawing.Point(564, 335);
            this.btnResearchNew.Name = "btnResearchNew";
            this.btnResearchNew.Size = new System.Drawing.Size(64, 28);
            this.btnResearchNew.TabIndex = 15;
            this.btnResearchNew.Text = "New Project";
            this.btnResearchNew.UseVisualStyleBackColor = true;
            this.btnResearchNew.Click += new System.EventHandler(this.btnResearchNew_Click);
            // 
            // lbClasses
            // 
            this.lbClasses.FormattingEnabled = true;
            this.lbClasses.ItemHeight = 20;
            this.lbClasses.Location = new System.Drawing.Point(12, 115);
            this.lbClasses.Name = "lbClasses";
            this.lbClasses.Size = new System.Drawing.Size(164, 104);
            this.lbClasses.TabIndex = 16;
            this.lbClasses.SelectedIndexChanged += new System.EventHandler(this.lbClasses_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 17;
            this.label4.Text = "Classes";
            // 
            // btnUpdateProject
            // 
            this.btnUpdateProject.Location = new System.Drawing.Point(634, 335);
            this.btnUpdateProject.Name = "btnUpdateProject";
            this.btnUpdateProject.Size = new System.Drawing.Size(73, 28);
            this.btnUpdateProject.TabIndex = 18;
            this.btnUpdateProject.Text = "Update";
            this.btnUpdateProject.UseVisualStyleBackColor = true;
            this.btnUpdateProject.Click += new System.EventHandler(this.btnUpdateProject_Click);
            // 
            // btnDeleteProject
            // 
            this.btnDeleteProject.Location = new System.Drawing.Point(713, 335);
            this.btnDeleteProject.Name = "btnDeleteProject";
            this.btnDeleteProject.Size = new System.Drawing.Size(69, 28);
            this.btnDeleteProject.TabIndex = 19;
            this.btnDeleteProject.Text = "Delete";
            this.btnDeleteProject.UseVisualStyleBackColor = true;
            this.btnDeleteProject.Click += new System.EventHandler(this.btnDeleteProject_Click);
            // 
            // cbNewClasses
            // 
            this.cbNewClasses.FormattingEnabled = true;
            this.cbNewClasses.Location = new System.Drawing.Point(565, 127);
            this.cbNewClasses.Name = "cbNewClasses";
            this.cbNewClasses.Size = new System.Drawing.Size(151, 28);
            this.cbNewClasses.TabIndex = 20;
            this.cbNewClasses.SelectedIndexChanged += new System.EventHandler(this.cbNewClasses_SelectedIndexChanged);
            // 
            // btnRemoveClass
            // 
            this.btnRemoveClass.Location = new System.Drawing.Point(565, 196);
            this.btnRemoveClass.Name = "btnRemoveClass";
            this.btnRemoveClass.Size = new System.Drawing.Size(151, 32);
            this.btnRemoveClass.TabIndex = 21;
            this.btnRemoveClass.Text = "Remove Class";
            this.btnRemoveClass.UseVisualStyleBackColor = true;
            this.btnRemoveClass.Click += new System.EventHandler(this.btnRemoveClass_Click);
            // 
            // btnAddClass
            // 
            this.btnAddClass.Location = new System.Drawing.Point(565, 161);
            this.btnAddClass.Name = "btnAddClass";
            this.btnAddClass.Size = new System.Drawing.Size(151, 29);
            this.btnAddClass.TabIndex = 22;
            this.btnAddClass.Text = "Add class to student";
            this.btnAddClass.UseVisualStyleBackColor = true;
            this.btnAddClass.Click += new System.EventHandler(this.btnAddClass_Click);
            // 
            // btnJSON
            // 
            this.btnJSON.Location = new System.Drawing.Point(770, 18);
            this.btnJSON.Name = "btnJSON";
            this.btnJSON.Size = new System.Drawing.Size(117, 64);
            this.btnJSON.TabIndex = 23;
            this.btnJSON.Text = "JSON";
            this.btnJSON.UseVisualStyleBackColor = true;
            this.btnJSON.Click += new System.EventHandler(this.btnJSON_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(244, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 20);
            this.label5.TabIndex = 24;
            this.label5.Text = "Student information";
            // 
            // btnNewStudent
            // 
            this.btnNewStudent.Location = new System.Drawing.Point(401, 18);
            this.btnNewStudent.Name = "btnNewStudent";
            this.btnNewStudent.Size = new System.Drawing.Size(121, 29);
            this.btnNewStudent.TabIndex = 25;
            this.btnNewStudent.Text = "New Student";
            this.btnNewStudent.UseVisualStyleBackColor = true;
            this.btnNewStudent.Click += new System.EventHandler(this.btnNewStudent_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 459);
            this.Controls.Add(this.btnNewStudent);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnJSON);
            this.Controls.Add(this.btnAddClass);
            this.Controls.Add(this.btnRemoveClass);
            this.Controls.Add(this.cbNewClasses);
            this.Controls.Add(this.btnDeleteProject);
            this.Controls.Add(this.btnUpdateProject);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbClasses);
            this.Controls.Add(this.btnResearchNew);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbStudentClasses);
            this.Controls.Add(this.lblResearchProject);
            this.Controls.Add(this.tbResearchProject);
            this.Controls.Add(this.cbStudentSupervisor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdateStudent);
            this.Controls.Add(this.btnAddStudent);
            this.Controls.Add(this.tbSubject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbStudents);
            this.Controls.Add(this.lDOB);
            this.Controls.Add(this.dtpDOB);
            this.Controls.Add(this.tbLastname);
            this.Controls.Add(this.lbFirstName);
            this.Controls.Add(this.lbLastName);
            this.Controls.Add(this.lbSubject);
            this.Controls.Add(this.tbFirstname);
            this.Controls.Add(this.lSupervisor);
            this.Controls.Add(this.cbSupervisors);
            this.Name = "Form1";
            this.Text = "Classes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbSupervisors;
        private System.Windows.Forms.Label lSupervisor;
        private System.Windows.Forms.TextBox tbFirstname;
        private System.Windows.Forms.Label lbSubject;
        private System.Windows.Forms.Label lbLastName;
        private System.Windows.Forms.Label lbFirstName;
        private System.Windows.Forms.TextBox tbLastname;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.Label lDOB;
        private System.Windows.Forms.ListBox lbStudents;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbSubject;
        private System.Windows.Forms.Button btnAddStudent;
        private System.Windows.Forms.Button btnUpdateStudent;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbStudentSupervisor;
        private System.Windows.Forms.TextBox tbResearchProject;
        private System.Windows.Forms.Label lblResearchProject;
        private System.Windows.Forms.ListBox lbStudentClasses;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnResearchNew;
        private System.Windows.Forms.ListBox lbClasses;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnUpdateProject;
        private System.Windows.Forms.Button btnDeleteProject;
        private System.Windows.Forms.ComboBox cbNewClasses;
        private System.Windows.Forms.Button btnRemoveClass;
        private System.Windows.Forms.Button btnAddClass;
        private System.Windows.Forms.Button btnJSON;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnNewStudent;
    }
}

