﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SupervisorAssignmentProgram.Model
{
    public class Student
    {
        //Primary key
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Subject { get; set; }
        
        //Foreign key to supervisor
        public int SupervisorID { get; set; }
        public Supervisor Supervisor { get; set; }

        //Many to many connection with classes, so students has a list of connection tables
        public ICollection<ClassStudent> ClassStudents { get; set; }

        //Each student have their own research project
        public ResearchProject ResearchProject { get; set; }
    }
}
