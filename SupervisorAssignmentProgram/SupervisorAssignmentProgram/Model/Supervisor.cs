﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SupervisorAssignmentProgram.Model
{
    public class Supervisor
    {
        //Primary key
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        
        //Each supervisor has a collection of students
        public ICollection<Student> Students { get; set; }
    }
}
