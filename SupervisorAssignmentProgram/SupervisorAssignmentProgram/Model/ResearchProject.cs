﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SupervisorAssignmentProgram.Model
{
    public class ResearchProject
    {
        public int ID { get; set; }
        public string Title { get; set; }

        //Foreign key to student
        public int StudentID { get; set; }
        public Student Student { get; set; }
    }
}
