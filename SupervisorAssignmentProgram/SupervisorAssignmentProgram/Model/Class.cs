﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SupervisorAssignmentProgram.Model
{
    public class Class
    {
        public int ID { get; set; }
        public string Name { get; set; }

        // Has a collection of the joining class, as it has a collection of students
        public ICollection<ClassStudent> ClassStudents { get; set; }
    }
}
