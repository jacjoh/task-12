﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SupervisorAssignmentProgram.Model
{
    public class ClassStudent
    {
        //Joining class for class and student where there are fereign keys to both
        public Student Student { get; set; }
        public Class Class{ get; set; }
        public int StudentID { get; set; }
        public int ClassID { get; set; }
    }
}
