﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SupervisorAssignmentProgram.Model
{
    public class SupervisorsDbContext : DbContext
    {
        public DbSet<Supervisor> Supervisors { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<ResearchProject> ResearchProjects { get; set; }
        public DbSet<ClassStudent> ClassStudents { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=PC7595\\SQLEXPRESS;" +
                "Initial Catalog=SupervisorDb;Integrated Security=True");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Defines the keys in class and student, and the joining class
            modelBuilder.Entity<ClassStudent>().HasKey(cl => new { cl.ClassID, cl.StudentID });
            modelBuilder.Entity<ClassStudent>()
                .HasOne(cl => cl.Class)
                .WithMany(c => c.ClassStudents)
                .HasForeignKey(cl => cl.ClassID);
            modelBuilder.Entity<ClassStudent>()
                .HasOne(cl => cl.Student)
                .WithMany(s => s.ClassStudents)
                .HasForeignKey(cl => cl.StudentID);

            //Specifies the foreign key in research project
            modelBuilder.Entity<ResearchProject>().HasKey(rp => new { rp.ID });
        }
    }
}
