# task-12
Allow for a university post grad administrator to assign supervisors to students to supervise them during their postgrad research degree

# Get started
To start you can run the DbScript.sql in Microsoft SQL Server Management Studio to download the database used. Then run InsertData.sql to add some data to the database. This is needed to run the windows forms program. To run the programs you just opens the file and then execute it.
The windows forms application is just to run in visual studio.

#The program
In the program you can add new students to the supervisors. The students have a research project, and are assigned to classes. it is possible to edit students, delete students, create research projects, edit them, delete them, assign classes to students, and remove classes from students.